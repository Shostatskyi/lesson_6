var studentsAndPoints = [
    'Алексей Петров', 0,
	'Ирина Овчинникова', 60,
	'Глеб Стукалов', 30,
	'Антон Павлович', 30, 
	'Виктория Заровская', 30, 
	'Алексей Левенец', 70, 
	'Тимур Вамуш', 30, 
	'Евгений Прочан', 60,
	'Александр Малов', 0
];
	
// 1. Вывести список студентов

console.log('Список студентов: ');

for (var i = 0, s=studentsAndPoints.length; i < s; i+=2) {
  console.log('Студент %s набрал %d баллов', studentsAndPoints[i], studentsAndPoints[i+1]);
}

// 2. Найти студента набравшего наибольшее количество баллов, и вывести информацию

var bestName, bestScore=0;

for (var i = 1, s=studentsAndPoints.length; i < s; i+=2) {
  if (studentsAndPoints[i] > bestScore) {
    bestScore=studentsAndPoints[i];
    bestName=studentsAndPoints[i-1];
  }
}

console.log('\nСтудент набравший максимальный балл: ');
console.log('Студент %s имеет максимальный балл: ', bestName, bestScore);

// 3. В группе появились новые студенты: «Николай Фролов» и «Олег Боровой». У них пока по 0 баллов. Добавить данные о них в массив.

studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);

// 4. Студенты «Антон Павлович» и «Николай Фролов» набрали еще по 10 баллов, внести изменения в массив.

var updateByTen = ['Антон Павлович', 'Николай Фролов', 'Дмитрий Фитискин']; 
 
for (var i = 0, j, s1=updateByTen.length, s2=studentsAndPoints.length; s1--; ++i) {
  j = studentsAndPoints.indexOf(updateByTen[i]);
  if (j > -1 && j < s2) {
  	studentsAndPoints[++j]+=10;
  }
}

// 5. Вывести список студентов, не набравших баллов:

console.log('\nСтуденты не набравшие баллов: ');

for (var i = 1, s=studentsAndPoints.length; i < s; i+=2) {
  if (studentsAndPoints[i] === 0)
    console.log(studentsAndPoints[i-1]);
}

// Дополнительное задание: Удалить из массива данные о студентах, набравших 0 баллов.

for(var i = 1, s=studentsAndPoints.length; s--; i += 2) {
  if(studentsAndPoints[i] === 0) {
    studentsAndPoints.splice(i-1, 2);
    i-=2;
  }
}

